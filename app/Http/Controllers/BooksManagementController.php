<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\BookCategory;
use Datatables;
use Image;
use Str;
use Storage;
use Carbon\Carbon;

class BooksManagementController extends Controller
{
    public function index()
    {
        return view('books_management.index');
    }

    public function listOfData()
    {
        $booksManagement = Book::with('bookCategory')->latest()->get();
        $rows  = array();

        foreach ($booksManagement as $key => $book) {
            $row                = array();
            $row['id']          = $key+1;
            $row['title']       = $book->title;
            $row['category']    = $book->bookCategory->category ?? 'Has Not Category';
            $row['isbn']        = $book->isbn;
            $row['author']      = $book->author;
            $row['page_count']  = $book->page_count;
            $row['language']    = $book->language;
            $row['description'] = $book->description;
            $row['path_cover']  = '<img src = "'. Storage::disk('public')->url('/books/'. $book->path_cover) .'" class = "rounded" width = "120">';
            $row['action'] = '
                    <div class="btn-group-sm btn-group">
                        <button data-toggle="remove-validate" class="btn btn-primary btn-shadow" onclick="editForm('. $book->id .')">
                            <i class="fas fa-edit btn-icon-wrapper"></i>
                        </button>
                        <button type="button" class="btn btn-danger btn-shadow" onclick="removeBook('. $book->id. ')"><i class="fas fa-trash-alt"></i></button>
                    </div>
            ';
            $rows[]             = $row;
        }

        return Datatables::of($rows)->escapeColumns([])->make(true);
    }

    public function store(Request $request)
    {
        $bookInput = $this->validate($request, $this->rules());
        if ($request->hasFile('path_cover')) {
            $pathCover = $this->saveFile('books', "{$request->title}", $request->file('path_cover'));
            
        } else $pathCover = 'cover-1.png';
        $bookInput['path_cover'] = $pathCover;

        Book::create($bookInput);
        return response()->json([
            'message' => "Book with the title :{$request->title} successfully added."
        ]);
    }

    public function edit(Book $book)
    {
        echo json_encode($book);
    }

    public function update(Request $request, Book $book)
    {
        $bookInput = $this->validate($request, $this->rules('nullable'));
        if ($request->hasFile('path_cover')) {
            $pathCover = $this->saveFile('books', "{$request->title}", $request->file('path_cover'));
            isset($pathCover) ? $this->removeFile('books', $book->path_cover) : '';
            
        } else $pathCover = $book->path_cover;
        $bookInput['path_cover'] = $pathCover;
        
        $book->update($bookInput);
        return response()->json([
            'message' => "Book with the title :{$request->title} successfully updated."
        ]);
    }

    public function destroy(Book $book)
    {
        isset($book->path_cover) ? $this->removeFile('books', $book->path_cover) : '';
        $book->delete();
        return response(null, 204);
    }

    public function categories()
    {
        $categories = BookCategory::select('id', 'category')->get();
        echo json_encode($categories);
    }

    private function rules($pathCover = 'required')
    {
        return [
            'title'       => 'required|min:6',
            'category_id' => 'required|integer|exists:book_categories,id',
            'isbn'        => 'required',
            'author'      => 'required|min:6',
            'page_count'  => 'required|integer',
            'language'    => 'required',
            'description' => 'required',
            'path_cover'  => $pathCover.'|image|mimes:jpeg,jpg,png'
        ];
    }

    protected function saveFile($directory = null, $fileName, $fileImage)
    {
        $fileName = Str::slug($fileName).'-'. Carbon::now()->toDateTimeString() .'.'. $fileImage->getClientOriginalExtension();
        if (!Storage::disk('public')->exists($directory)) {
            Storage::disk('public')->makeDirectory($directory);
        }
        $fileImage = Image::make($fileImage)->save();
        Storage::disk('public')->put("$directory/". $fileName, $fileImage);
        
        return $fileName;
    }

    protected function removeFile($directory = null, $fileName = 'photo')
    {
        return Storage::disk('public')->delete("/$directory//". $fileName);
    }
}
