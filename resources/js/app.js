import bootstrap 		from './bootstrap';
import dt               from 'datatables.net';
import dt4              from 'datatables.net-bs4';
import responsiveBs4    from 'datatables.net-responsive-bs4';
import swal             from 'sweetalert2';
import validate         from 'jquery-validation';

window.Swal = swal;