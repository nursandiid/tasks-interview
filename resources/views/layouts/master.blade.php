<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} &mdash; @yield('title')</title>
    
    <link rel="icon" href="/images/logo.png">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition layout-top-nav">
    <div class="wrapper">
        
        @include('layouts.partials.navbar')
        
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Books Management</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="content">
                <div class="container">
                    @yield('content')
                </div>
            </div>
        </div>

        @includeIf('layouts.partials.footer')
    </div>

    <script src="/js/app.js"></script>
    @stack('scripts')
</body>
</html>