<footer class="main-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- To the right -->
				<div class="float-right d-none d-sm-inline">
					Anything you want
				</div>
				<!-- Default to the left -->
				<strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights
				reserved.
			</div>
		</div>
	</div>
</footer>